package com.mastertech.microservice_pergunta.logs;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class PerguntaLogs {

	private static final String TOPIC = "questions";
	private static final String SERVERS = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";
//	private static final int P_THREADS = 1;

	public static KafkaProducer<String, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "a.Produtor - Perguntas");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<String, String> producer = new KafkaProducer<>(props);
		return producer;
	}

	public void GeraLog(String novoLog) {
//	public static Thread createProducerThread(String novoLog) {
//		Thread t = new Thread(new Runnable() {

//			@Override
//			public void run() {
//				System.out.printf("Thread [%s] starting.\n", Thread.currentThread().getName());
				Random random = new Random();
				KafkaProducer<String, String> producer = createProducer();

				final String topico = TOPIC;
				final String key = "a.log."+random.nextInt(10000000);
				final String body = novoLog;

				ProducerRecord<String, String> record = new ProducerRecord<>(topico, key, body);

				try {
					producer.send(record).get();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				producer.flush();
				producer.close();
			}
//		});
//		t.setName("ProducerThread-A-Perguntas");
//		return t;
	}

//	public void GeraLog(String novoLog) {
//		Thread t = createProducerThread(novoLog);
//		t.start();
//		System.out.println("PERGUNTAS");
//	}

package com.mastertech.microservice_pergunta.events;

import java.util.ArrayList;
import java.util.HashMap;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import org.springframework.beans.factory.annotation.Autowired;
import com.mastertech.microservice_pergunta.logs.PerguntaLogs;
import com.mastertech.microservice_pergunta.models.Pergunta;
import com.mastertech.microservice_pergunta.repositories.PerguntaRepository;

public class QuestionsListener implements MessageListener {

	@Autowired
	PerguntaRepository perguntaRepository;

	Session session;
	
	PerguntaLogs perguntaLogs = new PerguntaLogs();
	
	public QuestionsListener(Session session, PerguntaRepository perguntaRepository) {
		this.session = session;
		this.perguntaRepository = perguntaRepository;
	}
	
	@Override
	public void onMessage(Message request) {
		MapMessage requestMap = (MapMessage) request;
				
		try {
			int qtde = Integer.parseInt(requestMap.getString("quantity"));
			ArrayList<Pergunta> listaTodasPerguntas = (ArrayList<Pergunta>) perguntaRepository.findAll();
						
			MapMessage response = session.createMapMessage();
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			
			String logNovaReq= "Nova requisicao de perguntas - Qtde: "+ qtde;
			perguntaLogs.GeraLog(logNovaReq);
			
			ArrayList<HashMap<String,String>> listQuestions = new ArrayList<HashMap<String,String>>();
						
			for (int i = 0; i < qtde; i++) {
				int numero = (int) Math.floor(Math.random() * listaTodasPerguntas.size());
				Pergunta perguntaSorteada = new Pergunta();
				HashMap<String,String> mapQuestion = new HashMap<String,String>();
				perguntaSorteada = listaTodasPerguntas.remove(numero);
				mapQuestion.put("title", perguntaSorteada.getTitulo());
				mapQuestion.put("category", perguntaSorteada.getCategoria());
				mapQuestion.put("option1", perguntaSorteada.getOpcao1());
				mapQuestion.put("option2", perguntaSorteada.getOpcao2());
				mapQuestion.put("option3", perguntaSorteada.getOpcao3());
				mapQuestion.put("option4", perguntaSorteada.getOpcao4());
				mapQuestion.put("answer", String.valueOf(perguntaSorteada.getResposta()));
				listQuestions.add(mapQuestion);
			} 
			response.setObject("questions", listQuestions);
			producer.send(response);
			requestMap.acknowledge();
			producer.close();
			
			String logFinalReq = "Requisicao de perguntas respondida com sucesso";
			perguntaLogs.GeraLog(logFinalReq);
			
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}

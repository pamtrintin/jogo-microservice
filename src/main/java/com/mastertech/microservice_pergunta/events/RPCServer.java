package com.mastertech.microservice_pergunta.events;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mastertech.microservice_pergunta.repositories.PerguntaRepository;

@Component
public class RPCServer {
	private static String QUEUE_NAME = "a.queue.questions.full";
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";
	
	@Autowired
	PerguntaRepository perguntaRepository;
	
	@Autowired
	public void configuration() {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
        	// Criar uma sessão
			Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
//	Para entregar a mensagem e já dar OK na mensagem: 
//        	Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//	Para o consumidor dar OK na mensagem: 
//        	Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
//	Consumidor deve dar o comando abaixo após processar a mensagem
//        	message.acknowledge();
        	
        	// A fila que queremos popular
        	Queue queue = session.createQueue(QUEUE_NAME);
        	
        	MessageConsumer consumer = session.createConsumer(queue);
        	consumer.setMessageListener(new QuestionsListener(session, perguntaRepository));
		} catch (JMSException ex) {
			ex.printStackTrace();
		}
	}
}
